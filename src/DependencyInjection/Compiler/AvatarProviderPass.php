<?php

namespace App\DependencyInjection\Compiler;

use App\Registry\AvatarProviderRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AvatarProviderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(AvatarProviderRegistry::class)) {
            return;
        }

        $definition = $container->findDefinition(AvatarProviderRegistry::class);

        $taggedServices = $container->findTaggedServiceIds('app.avatar_provider');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addProvider', array(new Reference($id)));
        }
    }
}