<?php

namespace App\Services;

use App\Model\AvatarFactory;
use App\Services\AvatarProvider\AvatarProviderInterface;
use App\Registry\AvatarProviderRegistry;
use Doctrine\Common\Collections\ArrayCollection;

class AvatarService
{
    /**
     * @var AvatarProviderRegistry
     */
    private $avatarProviderRegistry;

    /**
     * @var AvatarFactory
     */
    private $avatarFactory;

    /**
     * AvatarService constructor.
     * @param AvatarProviderRegistry $avatarProviderRegistry
     * @param AvatarFactory $avatarFactory
     */
    public function __construct(AvatarProviderRegistry $avatarProviderRegistry, AvatarFactory $avatarFactory)
    {
        $this->avatarProviderRegistry = $avatarProviderRegistry;
        $this->avatarFactory = $avatarFactory;
    }

    public function getAvatars(string $email): ArrayCollection
    {
        $avatarCollection = new ArrayCollection();

        /** @var AvatarProviderInterface $avatarProvider */
        foreach ($this->avatarProviderRegistry->getProviders() as $avatarProvider) {
            if ($avatarProvider->isAvatarExist($email)) {
                $avatarCollection->add($this->avatarFactory->create($avatarProvider, $email));
            }
        }

        return $avatarCollection;
    }
}