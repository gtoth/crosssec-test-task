<?php

namespace App\Services\AvatarProvider;

class Libravatar extends AbstractAvatarProvider
{
    const PROVIDER_NAME = 'Libravatar';

    public function getAvatarUrl(string $email): string
    {
        $url = 'http://cdn.libravatar.org/avatar/';
        $url .= md5(strtolower(trim($email)));
        // 404 added explicitly for checking image existence
        $url .= sprintf("?s=%d&d=404", $this->getAvatarSize());

        return $url;
    }
}