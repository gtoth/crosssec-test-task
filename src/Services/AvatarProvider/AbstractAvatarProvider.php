<?php

namespace App\Services\AvatarProvider;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

abstract class AbstractAvatarProvider implements AvatarProviderInterface
{
    const PROVIDER_NAME = null;

    /**
     * @var int
     */
    private $avatarSize;

    public function __construct(int $avatarSize)
    {
        $this->avatarSize = $avatarSize;
    }

    abstract public function getAvatarUrl(string $email): string;

    public function getProviderName(): string
    {
        return static::PROVIDER_NAME;
    }

    public function isAvatarExist(string $email): bool
    {
        $client = new Client();

        try {
            $client->head($this->getAvatarUrl($email));
        } catch (ClientException $e) {
            return false;
        }

        return true;
    }

    protected function getAvatarSize(): int
    {
        return $this->avatarSize;
    }
}
