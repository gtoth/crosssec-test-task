<?php

namespace App\Services\AvatarProvider;

use emberlabs\GravatarLib\Gravatar as GravatarLib;

class Gravatar extends AbstractAvatarProvider
{
    const PROVIDER_NAME = 'Gravatar';

    public function getAvatarUrl(string $email): string
    {
        $gravatar = new GravatarLib();
        // 404 added explicitly for checking image existence
        $gravatar->setDefaultImage('404');
        $gravatar->setAvatarSize($this->getAvatarSize());

        return htmlspecialchars_decode($gravatar->get($email));
    }
}