<?php

namespace App\Services\AvatarProvider;

interface AvatarProviderInterface
{
    public function getProviderName(): string;
    public function getAvatarUrl(string $email): string;
    public function isAvatarExist(string $email): bool;
}