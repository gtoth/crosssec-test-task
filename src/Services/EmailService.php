<?php

namespace App\Services;

use App\Entity\Email;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;

class EmailService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AvatarEmailService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEmail(Email $avatarEmail) {
        if (empty($avatarEmail->getEmail())) {
            $avatarEmail->setEmail(Factory::create()->email);
        }

        $this->entityManager->persist($avatarEmail);
        $this->entityManager->flush();
    }

    public function getEmails()
    {
        $avatarEmailRepository = $this->entityManager->getRepository(Email::class);
        return $avatarEmailRepository->findBy([], ['id' => 'desc']);
    }
}