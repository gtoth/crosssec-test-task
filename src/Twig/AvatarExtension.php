<?php

namespace App\Twig;

use App\Services\AvatarService;
use Twig\Extension\AbstractExtension;

class AvatarExtension extends AbstractExtension
{
    /**
     * @var AvatarService
     */
    private $avatarService;

    /**
     * AvatarExtension constructor.
     * @param AvatarService $avatarService
     */
    public function __construct(AvatarService $avatarService)
    {
        $this->avatarService = $avatarService;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('avatars', [$this, 'getAvatars'])
        ];
    }

    public function getAvatars($email)
    {
        return $this->avatarService->getAvatars($email);
    }
}