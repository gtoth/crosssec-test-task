<?php

namespace App\Model;

use App\Services\AvatarProvider\AvatarProviderInterface;

class AvatarFactory
{
    public function create(AvatarProviderInterface $avatarProvider, string $email): Avatar
    {
        $avatar = new Avatar();
        $avatar->setAvatarUrl($avatarProvider->getAvatarUrl($email));
        $avatar->setProviderName($avatarProvider->getProviderName());

        return $avatar;
    }
}