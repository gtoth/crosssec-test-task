<?php

namespace App\Registry;

use App\Services\AvatarProvider\AvatarProviderInterface;

class AvatarProviderRegistry
{
    /**
     * @var array
     */
    private $avatarProviders = [];

    public function addProvider(AvatarProviderInterface $avatarProvider)
    {
        $this->avatarProviders[] = $avatarProvider;
    }

    /**
     * @return array
     */
    public function getProviders() {
        return $this->avatarProviders;
    }
}