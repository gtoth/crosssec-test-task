<?php

namespace App\Controller;

use App\Form\Type\EmailFormType;
use App\Services\EmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AvatarController extends AbstractController
{
    /**
     * @var EmailService
     */
    private $avatarEmailService;

    /**
     * AvatarController constructor.
     * @param EmailService $avatarEmailService
     */
    public function __construct(EmailService $avatarEmailService)
    {
        $this->avatarEmailService = $avatarEmailService;
    }

    public function create(Request $request)
    {
        $emailForm = $this->createForm(EmailFormType::class);

        $emailForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $email = $emailForm->getData();

            $this->avatarEmailService->addEmail($email);

            $this->addFlash(
                'success',
                'Email added!'
            );

            return $this->redirectToRoute('avatar_success');
        }

        return $this->render('avatar_create.html.twig', ['emails' => $this->avatarEmailService->getEmails(), 'emailForm' => $emailForm->createView()]);
    }

    public function success()
    {
        return $this->render('avatar_success.html.twig');
    }
}
