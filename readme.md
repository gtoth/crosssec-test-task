# Crosssec Test Task
### Description
This is a simple tool designed to show avatars associated with given email addresses from the predefined providers.
  
### Requirements
    - composer
    - yarn
    
### Installation
Create .env from .env.dist and add your parameters, then
```sh
$ composer install
$ php bin/console doctrine:database:create
$ php bin/console --no-interaction doctrine:migrations:migrate 
$ yarn install
$ yarn encore dev 
$ php bin/console server:run
```

### Providers
By default, there are two providers defined: Gravatar and Libravatar.
You can easily add new provider by creating a new class that extends "App\Services\AvatarProvider\AbstractAvatarProvider" and define it as a service with 'app.avatar_provider' tag and $avatarSize argument:

```sh
<?php
  
namespace App\Services\AvatarProvider;
  
class NewProvider extends App\Services\AvatarProvider\AbstractAvatarProvider
{
    const PROVIDER_NAME = 'New provider';  
      
    public function getAvatarUrl(string $email): string
    {
        ...
        return $url;
    }
}
 ```
 
 ```sh
 # config/services.yaml
parameters:
    avatar_provider.new_provider.image_size: 80
  
services:
    app.avatar_provider.new_provider:
         class: App\Services\AvatarProvider\NewProvider
         arguments:
             $avatarSize: '%avatar_provider.new_provider.image_size%'
         tags:
             - {name: 'app.avatar_provider'}
 ```